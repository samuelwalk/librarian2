const express        = require('express');
const app            = express();
const routes		 = require('./app/routes');

const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
const db             = require('./config/db');
require('dotenv').config();

app.use(bodyParser.urlencoded({ extended: true }));


MongoClient.connect(db.url, (err, database) => {
	if (err) return console.log(err);

	const piiDb = database.db('pii-hackathon');
	routes(app, piiDb);

	app.listen(process.env.PORT, () => {
		console.log('Listening on port ' + process.env.PORT);
	});
});