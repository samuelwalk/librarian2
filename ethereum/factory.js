const web3 = require("./web3");
const CampaignFactory = require("./build/CampaignFactory.json");

const instance = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  "0xA145CD1774E95253C5f27e620f9fC331D7CA9750"
);

module.exports = instance;
