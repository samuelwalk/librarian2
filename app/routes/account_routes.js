const factory = require("../../ethereum/factory");
const web3 = require("../../ethereum/web3");
const Campaign = require("../../ethereum/build/Campaign");
const errMsgObj = { error: "An error has occurred" };

module.exports = function(app, db) {
    app.get("/accounts", (req, res) => {
        console.log("GET /accounts");

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET");

        db
            .collection("accounts")
            .find({}, { _id: 0 })
            .toArray((err, results) => {
                if (err) throw err;
                maskForgottenAccounts(results).then(accounts => {
                    res.send(accounts);
                });
            });
    });

    app.get("/blockchainData", async (req, res) => {
        console.log("GET /blockchainData");

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET");

        let blockchainData = await getBlockchainData();
        res.send(blockchainData);
    });

    app.get("/recordToBlockchain", (req, res) => {
        console.log("GET /recordToBlockchain");

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET");

        db
            .collection("accounts")
            .find({}, { _id: 0 })
            .toArray((err, results) => {
                if (err) throw err;
                maskForgottenAccounts(results).then(maskedAccounts => {
                    recordTransaction(JSON.stringify(maskedAccounts))
                        .then(() => {
                            res.sendStatus(204);
                        })
                        .catch(reason => {
                            console.log(reason);
                        });
                });
            });
    });

    app.get("/recordSingleRecordToBlockchain/:accountNumber", (req, res) => {
        console.log("GET /recordToBlockchain");

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET");

        const accountNumber = req.params.accountNumber;
        db
            .collection("accounts")
            .find({ accountNumber: accountNumber }, { _id: 0 })
            .toArray(function(err, results) {
                if (err) throw err;
                maskForgottenAccounts(results).then(maskedAccounts => {
                    recordTransaction(JSON.stringify(maskedAccounts))
                        .then(() => {
                            res.sendStatus(204);
                        })
                        .catch(reason => {
                            console.log(reason);
                        });
                });
            });
    });

    app.get("/accounts", (req, res) => {
        console.log("GET /accounts");

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET");

        db
            .collection("accounts")
            .find({}, { _id: 0 })
            .toArray((err, results) => {
                if (err) throw err;
                maskForgottenAccounts(results).then(accounts => {
                    res.send(accounts);
                });
            });
    });

    app.get("/account/:accountNumber", (req, res) => {
        console.log("GET /account/:accountNumber");

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET");

        const accountNumber = req.params.accountNumber;
        db
            .collection("accounts")
            .find({ accountNumber: accountNumber }, { _id: 0 })
            .toArray(function(err, results) {
                if (err) throw err;
                maskForgottenAccounts(results).then(accounts => {
                    res.send(accounts);
                });
            });
    });

    app.get("/forget/:accountNumber", (req, res) => {
        console.log("FORGET /account/:accountNumber");

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET");

        const accountNumber = req.params.accountNumber;
        db
            .collection("forget_list")
            .insert({ accountNumber: accountNumber }, (err, results) => {
                if (err) {
                    res.send(errMsgObj);
                    throw err;
                } else {
                    res.send("Account " + accountNumber + " forgotten!");
                }
            });
    });

    app.get("/unforget", (req, res) => {
        console.log("unforgetAllAccounts");

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET");

        db.collection("forget_list").deleteMany({}, (err, results) => {
            if (err) {
                res.send(errMsgObj);
                throw err;
            } else {
                res.send("Accounts unforgotten!");
            }
        });
    });

    let maskForgottenAccounts = function(accounts) {
        return new Promise((resolve, reject) => {
            let forgottenAccounts = [];

            db
                .collection("forget_list")
                .find({}, { _id: 0 })
                .toArray((err, results) => {
                    if (err) throw err;
                    forgottenAccounts = results;
                    for (let i = 0; i < accounts.length; i++) {
                        for (let j = 0; j < forgottenAccounts.length; j++) {
                            if (
                                forgottenAccounts[j].accountNumber === accounts[i].accountNumber
                            ) {
                                accounts[i].accountNumber = "**REDACTED**";
                                accounts[i].name = "**REDACTED**";
                                accounts[i].address = "**REDACTED**";
                                accounts[i].phoneNumber = "**REDACTED**";
                                accounts[i].balance = "**REDACTED**";
                            }
                        }
                    }
                    resolve(accounts);
                });
        });
    };
};

async function recordTransaction(jsonResponse) {
    try {
        const accounts = await web3.eth.getAccounts();
        await factory.methods.createCampaign(jsonResponse).send({
            from: accounts[0],
            gas: 4712388
        });
        console.log("Transaction recorded from " + accounts[0]);
    } catch (err) {
        console.log(err);
    }
}

async function getBlockchainData() {
    try {
        const contractAddressArray = await factory.methods
            .getDeployedCampaigns()
            .call();
        const lastCreatedContractAddress = contractAddressArray[contractAddressArray.length - 1];
        const lastCreatedContract = getContractInstance(lastCreatedContractAddress);

        return lastCreatedContract.methods.jsonResponse().call();
    } catch (err) {
        console.log(err);
    }
}

function getContractInstance(address) {
    return new web3.eth.Contract(JSON.parse(Campaign.interface), address);
}
